# DevOps Engineer

## Introduction

Hello and thank you for considering the DevOps Engineer position at [Quandoo](https://www.quandoo.com/careers/)! To evaluate your technical skills in part of our stack and suitability for the role, we have created a task for you to complete.

## Task description

To complete this task, you require a full-stack web application. You are free to choose any of the implementations of [Real World](https://github.com/gothinkster/realworld) applications that you feel most comfortable with. Be sure to select the technology that you are most proficient in. Note that the application you choose will not be subject to evaluation.

### 1.1 Provision Infrastructure

As a DevOps Engineer, you want to write Terraform code for provisioning EKS cluster on AWS to deploy your chosen web application.

Acceptance Criteria:
- The cluster is ready to spin off your chosen web application.
- Terraform code is reusable and testable.

### 1.2 Deploy Application

As a DevOps Engineer, you want to write a Helm chart for deploying your chosen web application on your newly created EKS cluster with Helm.

Acceptance Criteria:
- The Helm chart includes the necessary subcharts to deploy dependencies.
- The Helm chart incorporates the required tests to ensure proper functionality.

### 1.3 Housekeeping

As a DevOps Engineer, you want to use a tool of your choice for automating the backup of the database and any mutable storage at least once daily.

Acceptance Criteria:
- The backups are safely stored in a storage of your choice other than the EKS cluster.

## Evaluation Criteria

The primary evaluation criteria revolve around your approach, as reflected in your commit history, to meet the aforementioned acceptance criteria. Furthermore, we will take into account the extent to which your solution demonstrates adaptability to changing needs and the ease with which it can accommodate future expansions or improvements.

## What to commit?

To complete the evaluation, please submit the following:

- All relevant pieces of code, including but not limited to Terraform and Helm.
- All relevant documentation related to your implementation. This may include deployment guides, configuration instructions, and any other supporting materials.

## Submitting your solution

**Please do not spend more than a few hours on this task**

-  Fork this repo with [Visibility level](https://docs.gitlab.com/ee/user/public_access.html) set to **Private**.
-  Commit your artifacts (e.g. document, diagrams) into the private repository in `feat/quandoo` branch. We encourage you to commit often so that we can see a history of trial and error rather than a single monolithic push.
-  Share the project with the gitlab users **ReeSilva_Quandoo**, **musaib.khan1** and **maateen** (go to **Project information -> Members -> Invite member**, find the user in **Select members to invite** and set **Choose a role permission** to **Developer**).
-  Create a merge request from `feat/quandoo` branch to `main` branch in the private repository and request review from **maateen**.